import Vue from 'vue'
import Page from '~/components/Page.vue'
import BigTeaser from '~/components/BigTeaser.vue'
import MediumTeaser from '~/components/MediumTeaser.vue'
import FeatureSection from '~/components/FeatureSection.vue'
import Grid from '~/components/Grid.vue'
import Recipe from '~/components/Recipe.vue'
import ImageHeader from '~/components/ImageHeader.vue'
import ProductGroups from '~/components/ProductGroups.vue'
import Settings from '~/components/Settings.vue'

Vue.component('blok-page', Page)
Vue.component('blok-big-teaser', BigTeaser)
Vue.component('blok-medium-teaser', MediumTeaser)
Vue.component('blok-feature-section', FeatureSection)
Vue.component('blok-grid', Grid)
Vue.component('blok-recipe', Recipe)
Vue.component('blok-image-header', ImageHeader)
Vue.component('blok-product-groups', ProductGroups)
Vue.component('blok-settings', Settings)
