import Vue from 'vue'

export default function ({ app, route, store, isDev }) {
  let version = route.query._storyblok || isDev ? 'draft' : 'published'
  let releaseId = route.query._storyblok_release

  if (!store.state.cacheVersion) {
    store.dispatch('loadCacheVersion')
  }

  if (!store.state.settings._uid) {
    return store.dispatch('loadSettings', {version: version, releaseId: releaseId})
  }
}
