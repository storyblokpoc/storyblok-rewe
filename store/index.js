import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      cacheVersion: '',
      settings: {
        main_navi: []
      },
      version: 'published'
    },
    mutations: {
      setSettings (state, settings) {
        state.settings = settings
      },
      setCacheVersion (state, version) {
        state.cacheVersion = version
      },
      setVersion (state, version) {
        state.version = version
      }
    },
    actions: {
      loadSettings ({ commit }, context) {
        commit('setVersion', context.version)

        return this.$storyapi.get(`cdn/stories/global`, {
          version: context.version,
          from_release: context.releaseId
        }).then((res) => {
          commit('setSettings', res.data.story.content)
        })
      },
      loadCacheVersion ({ commit }) {
        return this.$storyapi.get(`cdn/spaces/me`).then((res) => {
          commit('setCacheVersion', res.data.space.version)
        })
      }
    }
  })
}

export default createStore
